<?php

/**
* Classe responsavel pelos exercicios 1, 3 e 4
*/

require_once("banco.class.php");

class App {
	
	// exercicio 1
	function caixa_eletronico($valor) {
		$contador = function($valor, $nota) {
			if ($nota <= $valor) {
				$calc = $valor / $nota;
				$valor %= $nota;
				return $calc;
			}

			return 0;
		};

		$notas = array(
			'1' => $contador($valor, 1),
			'2' => $contador($valor, 2),
			'5' => $contador($valor, 5),
			'10' => $contador($valor, 10),
			'20' => $contador($valor, 20),
			'50' => $contador($valor, 50),
			'100' => $contador($valor, 100)
		);

		return $notas;	
	}

	// exercicio 3
	function contato($dados) {
		$cabecalho = array(
			"MIME-Version: 1.0",
			"Content-type: text/html; charset=iso-8859-1",
			"From: TOPNODE teste <teste@topnode.com.br>",
			"Reply-To: TOPNODE teste <teste@topnode.com.br>"
		);

		$cabecalho = implode("\r\n", $cabecalho);
		$msg = $dados["nome"] . "<br>" . $dados["email"] . "<br>" . $dados["cpf"] . "<br>" . $dados["telefone"] . "<br>" . $dados["celular"];

		if (mail("cliente@topnode.com.br", "Dados do cliente", $msg, $cabecalho)) {
			return "E-mail enviado com sucesso";
		}
		else {
			return "Erro ao enviar e-mail. Configure um SMTP Server";
		}
	}

	// exercicio 4

	// devolve dados no formato HTML
	function criaHTML($dados) {
		$ret = NULL;

		foreach ($dados as $dado => $valor) {
			$ret .= "<li>" . strtoupper("<b>" . $dado . "</b>") . ' - ' . $valor . "</li>";
		}

		return $ret;
	}

	// devolve dados no formato XML
	function criaXML($dados) {
		$doc = new DOMDocument("1.0", "ISO-8859-1");
		$doc->formatOutput = TRUE;

		$principal = $doc->createElement("dados");
		$empresa = $doc->createElement("empresa", $dados['empresa']);
		$site = $doc->createElement("site", $dados['site']);
		$data = $doc->createElement("data", $dados['data']);
		$url = $doc->createElement("url", $dados['url']);

		$principal->appendChild($empresa);
		$principal->appendChild($site);
		$principal->appendChild($data);
		$principal->appendChild($url);

		$doc->appendChild($principal);

		return $doc->saveXML();
	}

	function api($formato) {

		$dados = array(
			'empresa' => 'TOP(NODE)',
			'site' => 'topnode.com.br',
			'data' => date("d/m/Y H:i:s"),
			'url' => $_SERVER["SERVER_NAME"] . '/' . $_SERVER["REQUEST_URI"]
		);

		switch ($formato) {
			case 'json':
				return json_encode($dados);
				break;

			case 'html':
				return $this->criaHTML($dados);
				break;

			case 'xml':
				return array('cabecalho' => header("Content-Type: text/xml"), 'conteudo' => $this->criaXML($dados));
				break;

			default:
				echo "Formato inválido";
				break;
		}
	}
}


$app = new App();

// Recebe POST e calcula notas
if (!empty($_POST["valor"])) {
	$valor = $app->caixa_eletronico($_POST["valor"]);

	foreach ($valor as $n => $q) {
		if ($q >= 1) {
			echo "R$ " . $n . " = " . $q . " nota(s)<br>";
		}
	}
}

// Recebe email e consulta base de dados
if (!empty($_GET["email"])) {
	$banco = new Banco();
	$banco->busca_cliente($_GET["email"]);
}

// Recebe contato e chama metodo de envio por email
if (!empty($_POST["dados"])) {
	$envia_email = $app->contato($_POST["dados"]);
	echo $envia_email;
}

// Recebe GET request e retorna o formato solicitado
if (!empty($_GET["formato"])) {
	$formato = $app->api(strtolower($_GET["formato"]));

	if (is_array($formato)) {
		echo $formato['conteudo'];
	}
	else {
		echo $formato;
	}
}
