<?php

/**
* Classe responsavel pela interação com o banco de dados
*/

class Banco {

	// exercicio 2
	function __construct() {
		$this->mysql = new mysqli("localhost", "root", "", "cotacao_interview", 3306);
	}

	function busca_cliente($email) {
		// busca o pedido
		$query = $this->mysql->query("
			SELECT pedido.id, cliente.nome as cliente, produto.nome as produto, SUM(produto_valor.valor) as total 
			FROM cliente, vidas, pedido, produto, produto_valor 
			WHERE cliente.id = vidas.cliente_id
			AND cliente.id = pedido.cliente_id
			AND produto.id = pedido.produto_id
			AND produto.id = produto_valor.produto_id
			AND cliente.email = '$email'
			GROUP BY pedido.id
		");

		// monta e exibe tabela com o pedido
		echo "<table class='table table-bordered'><tr><th>ID Pedido</th><th>Cliente</th><th>Produto</th><th>Total</th></tr>";

		while ($dados = $query->fetch_assoc()) {
			echo "<tr><td>" . $dados["id"] . "</td>";
			echo "<td>" . $dados["cliente"] . "</td>";
			echo "<td>" . $dados["produto"] . "</td>";
			echo "<td>R$ " . $dados["total"] . "</td></tr>";
		}

		echo "</table>";
	}
}
