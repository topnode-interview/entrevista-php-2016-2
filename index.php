<?php require_once("includes/header.html"); ?>

<form action="#" id="caixa" class="esconde" onsubmit="calcula_notas(); javascript: return false;">
  <div class="form-inline">
    <h3>1. Caixa</h3>
    <input type="number" class="form-control" id="valor" placeholder="Digite o valor" required />
    <button type="submit" class="btn btn-success">Consultar</button>
  </div>
</form>

<div id="valores"></div>

<form action="#" id="busca" class="esconde" onsubmit="pedidos(); javascript: return false;">
  <div class="form-inline">
    <h3>2. Busca</h3>
    <input type="email" class="form-control" id="email" placeholder="Digite um e-mail válido" required />
    <button type="submit" class="btn btn-success">Pesquisar</button>
  </div>
</form>

<div id="valor_pedido"></div>

<form action="#" id="contato" class="esconde" onsubmit="envia_email(); javascript: return false;">
  <h3>3. Contato</h3>

  <div class="form-group">
    <label for="contato_n">Nome</label>
    <input type="text" class="form-control" id="contato_n" placeholder="Digite o nome completo" required />
  </div>

  <div class="form-group">
    <label for="contato_e">E-mail</label>
    <input type="email" class="form-control" id="contato_e" placeholder="Digite um e-mail válido" required />
    <span id="msg_mail" class="esconde">E-mail invalido</span>
  </div>

  <div class="form-group">
    <label for="contato_c">CPF</label>
    <input type="text" class="form-control" id="contato_c" placeholder="Digite seu CPF" required />
  </div>

  <div class="form-group">
    <label for="contato_t">Telefone</label>
    <input type="text" class="form-control" id="contato_t" placeholder="Deixe seu contato" required />
  </div>

  <div class="form-group">
    <label for="contato_t2">Celular</label>
    <input type="text" class="form-control" id="contato_t2" placeholder="Deixe seu contato" required />
  </div>

  <div class="form-group">
    <button type="submit" class="btn btn-success">Enviar</button>
    <button type="reset" class="btn btn-default">Cancelar</button>
  </div>
</form>

<form action="src/app.class.php" method="get" id="api" class="esconde">
  <div class="form-group">
    <h3>4. API</h3>
    <select class="form-control" name="formato" required>
      <option value="">Selecione</option>
      <option value="json">JSON</option>
      <option value="html">HTML</option>
      <option value="xml">XML</option>
    </select><br>
    <button type="submit" class="btn btn-success">Buscar</button>
  </div>
</form>
