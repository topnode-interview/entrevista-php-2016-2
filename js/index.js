function mostra(id) {

	// mostra conteudo da pagina conforme clique
	if (id == "item1") {
		$("#caixa, #valores").show('slow');
		$("#contato, #busca, #valor_pedido, #api").hide('slow');
	}
	else if (id == "item2") {
		$("#busca, #valor_pedido").show('slow');
		$("#caixa, #valores, #contato, #api").hide('slow');
	}
	else if (id == "item3") {
		$("#contato").show('slow');
		$("#caixa, #valores, #busca, #valor_pedido, #api").hide('slow');
	}
	else {
		$("#api").show('slow');
		$("#caixa, #valores, #busca, #valor_pedido, #contato").hide('slow');
	}
}

// faz o POST do valor via AJAX
function calcula_notas() {
	$.post('http://localhost/entrevista-php-2016-2/src/app.class.php', {valor: $("#valor").val()}, function(data) {
		$("#valores").html('<br>' + data);
	}).fail(function() {
		window.alert("Erro ao calcular valor");
	});
}

// busca dados do pedido no SERVER
function pedidos() {
	$.get('http://localhost/entrevista-php-2016-2/src/app.class.php', {email: $("#email").val()}, function(data) {
		$("#valor_pedido").html('<br><h3>Dados do pedido</h3>' + data);
	}).fail(function() {
		window.alert("Erro ao realizar pesquisa");
	});
}

// envia os dados para o SERVER disparar o e-mail
function envia_email() {
	var dados = {
		nome: $("#contato_n").val(),
		email: $("#contato_e").val(),
		cpf: $("#contato_c").val(),
		telefone: $("#contato_t").val(),
		celular: $("#contato_t2").val()
	};

	// valida e-mal
	if (dados.email.indexOf(".") == -1) {
		$("#msg_mail").css("color", "#ff0000").show();
		return false;
	}

	// POST request
	$.post('http://localhost/entrevista-php-2016-2/src/app.class.php', {dados: dados}, function(data) {
		window.alert(data);
	}).fail(function() {
		window.alert("Erro ao transmitir dados");
	});
}

$(document).ready(function() {
	// valida nome e e-mail
	$("#contato_n").keyup(function() {
		var v = $(this).val();
		$(this).val(v.replace(/[^A-Za-zãõçáéó ]+/g, ""));
	});

	$("#contato_e").keyup(function() {
		var v = $(this).val();
		$(this).val(v.replace(/[^a-z0-9.@_]+/g, ""));
	});

	// valida CPF e telefones
	$("#contato_c").mask("999.999.999-99");
	$("#contato_t").mask("(99) 9999-9999");
	$("#contato_t2").mask("(99) 9 9999-9999");
});
